import React, { useState } from 'react';
import PropTypes from 'prop-types';
import i18n from 'meteor/universe:i18n';
import { Link, useHistory } from 'react-router-dom';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Tooltip from '@mui/material/Tooltip';
import TablePagination from '@mui/material/TablePagination';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import EditIcon from '@mui/icons-material/Edit';
import ClearIcon from '@mui/icons-material/Clear';
import { Typography } from '@mui/material';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

import EnhancedTableHead from '../packTable/tableHead';
import { useAppContext } from '../../contexts/context';
import PackDelete from './packDelete';
import IconButtonEole from '../buttons/iconButtonEole';

const typographieHeaderStyle = {
  width: '18rem',
  color: 'primary.light',
  textAlign: 'center',
};
const tableCellStyle = {
  color: 'primary.light',
  textAlign: 'center',
};
const iconStyle = {
  color: 'primary.light',
};

// eslint-disable-next-line no-unused-vars
function PackList({ packs, isUserPack }) {
  function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const [openModal, setOpenModal] = useState(false);
  const history = useHistory();
  const [{ userId }] = useAppContext();

  function getComparator(order, orderBy) {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  // This method is created for cross-browser compatibility, if you don't
  // need to support IE11, you can use Array.prototype.sort() directly
  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) {
        return order;
      }
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(25);
  const [currentPack, setCurrentPack] = React.useState({});

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleEditButton = (id) => {
    history.push(`/packs/edit/${id}`);
  };

  const handleDeleteButton = (pack) => {
    setOpenModal(true);
    setCurrentPack(pack);
  };

  return (
    <div style={{ height: 600 }}>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="pack table" sx={{ backgroundColor: 'primary.purple' }}>
          <EnhancedTableHead
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            isUserPack={isUserPack}
          />
          <TableBody>
            {stableSort(packs, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((pack) => {
                return (
                  <TableRow hover tabIndex={-1} key={pack._id} sx={{ backgroundColor: 'primary.lightPurple' }}>
                    <TableCell>
                      <span
                        style={{
                          textOverflow: 'ellipsis',
                          overflow: 'hidden',
                          width: 200,
                          display: 'block',
                          textAlign: 'center',
                        }}
                      >
                        <Typography variant="paragraph" sx={typographieHeaderStyle}>
                          {pack.name}
                        </Typography>
                      </span>
                    </TableCell>
                    {!isUserPack ? (
                      <TableCell sx={tableCellStyle}>{pack.ownerName}</TableCell>
                    ) : pack.isPublic ? (
                      <TableCell sx={tableCellStyle}>{pack.isPublic}</TableCell>
                    ) : (
                      <TableCell sx={tableCellStyle}>
                        <Tooltip title={i18n.__('components.PacksCard.privateIcon')} placement="top-start">
                          <VisibilityOffIcon fontSize="large" sx={iconStyle} />
                        </Tooltip>
                      </TableCell>
                    )}

                    <TableCell sx={tableCellStyle}>
                      <span style={{ textOverflow: 'ellipsis', overflow: 'hidden', width: 630, display: 'block' }}>
                        {pack.description}
                      </span>
                    </TableCell>
                    <TableCell sx={tableCellStyle}>{pack.applications.length}</TableCell>
                    <TableCell>
                      {pack.owner === userId ? (
                        <div>
                          <Link to={`/packs/detail/${pack._id}`}>
                            <IconButtonEole
                              tooltipText={i18n.__('components.PackList.detailTooltip')}
                              icon={<OpenInNewIcon />}
                              style={{ backgroundColor: 'none' }}
                            />
                          </Link>
                          <IconButtonEole
                            tooltipText={i18n.__('components.PacksCard.editPack')}
                            icon={<EditIcon />}
                            onClick={() => handleEditButton(pack._id)}
                            style={{ backgroundColor: 'none' }}
                          />
                          <IconButtonEole
                            tooltipText={i18n.__('components.PacksCard.deletePack')}
                            icon={<ClearIcon />}
                            onClick={() => handleDeleteButton(pack)}
                            style={{ backgroundColor: 'none' }}
                          />
                        </div>
                      ) : (
                        <Link to={`/packs/detail/${pack._id}`}>
                          <IconButtonEole
                            tooltipText={i18n.__('components.PackList.detailTooltip')}
                            icon={<OpenInNewIcon />}
                            style={{ backgroundColor: 'none' }}
                          />
                        </Link>
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50, 100]}
        component="div"
        count={packs.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      {openModal ? <PackDelete pack={currentPack} open={openModal} onClose={() => setOpenModal(false)} /> : null}
    </div>
  );
}

PackList.defaultProps = {
  isUserPack: false,
};

PackList.propTypes = {
  packs: PropTypes.arrayOf(PropTypes.object).isRequired,
  isUserPack: PropTypes.bool,
};

export default PackList;
